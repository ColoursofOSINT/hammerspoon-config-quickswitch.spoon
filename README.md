# hammerspoon-config-quickswitch.spoon

## This config spoon loads shortcuts for applications by uses hammerspoon

To use, download [hammerspoon](https://www.hammerspoon.org/), and load the spoon in using:

`hs.loadSpoon("quickswitch")
spoon.quickswitch:bindHotkeys({})`
